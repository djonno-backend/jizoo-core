import {Environment, Application} from '../src'

describe("Running configuration setup", () => {
    const configuration = {
        dev: {
            application: "test-app",
            env: Environment
        },
        prod: {
            application: "prod-app",
            env: Environment
        }
    }

    test("test configuration combo", () => {    
        Application.configure(configuration)        
        let result = Application.config("env")
        expect(result).toBeDefined()        
    })
})