import {Model, Environment, Application, Collection} from '../src'
import {EventEmitter} from 'events'
import { v4 as uuidv4 } from 'uuid'

describe("Testing for Model construct", () => {
    const config = {
        dev: {
            application: "test-deck", 
            env: Environment,
            security: {
                keys: {
                    vault: {
                        algorithm: "aes-256-cbc",
                        key: "$B&E)H@McQfTjWnZr4u7x!z%C*F-JaNd"
                    }
                }
            }
        }
    }

    class TestClass extends Model {

        public testerValue: number = 0    
        protected someEvent: EventEmitter

        protected someEventHandler(data: TestClass) {    
            data.testerValue += 1
        }

        protected prepare() : void {
            super.prepare()
            this.rules.set("test1", { type: "text" })
            this.rules.set("test2", { type: "number" })
            this.rules.set("test3", { type: "text", limits: ["limiting", "someoption"]})
            this.rules.set("test4", { type: "yesno", required: true})
            this.rules.set("test5", { type: "text", required: true})
            this.rules.set("test6", { type: "text", encrypted: true})

            this.defaults["test4"] = true

            this.someEvent = new EventEmitter()
            this.someEvent.on("fire", this.someEventHandler)
        }

        public get someValue() : number {
            return this.testerValue
        }

        public caller() {
            this.someEvent.emit("fire", this)
        }
    }

    Application.configure(config)

    test("Test event emitter trigger", () => {
        let tester = new TestClass()
        tester.caller()
        expect(tester.someValue).toBe(1)
    })
    test("Getting and setting of data", () => {
        let tester = new TestClass() 
        tester.set("test1", "value")
        tester.set("test2", 1)
        tester.set("test3", "limiting")
        expect(tester).toBeDefined()
        expect(tester.isDirty).toBe(true)
        expect(tester.get("test1")).toBe("value")        
        expect(tester.get("test2")).toBe(1)
        expect(tester.get("test3")).toBe("limiting")
        expect(tester.get("test4")).toBe(true)
    })
    test("Testing of bad data states", () => {
        let tester = new TestClass() 
        try {
            tester.set("test3", "unknown")
        } catch (ex) {
            expect(ex).toBeInstanceOf(Error)
        }        
    })
    test("Testing of bad data required state", () => {
        let tester = new TestClass() 
        try {
            tester.set("test5", undefined)
        } catch (ex) {
            expect(ex).toBeInstanceOf(Error)
        }        
    })
    test("data encryption into item", () => {        
        const value = "example.data"
        let tester = new TestClass() 
        tester.set("test6", value)
        expect(tester).toBeDefined()
        expect(tester.isDirty).toBe(true)
        expect(tester.get("test6")).toBe(value)
    })
    test("data import", () => {
        const data = {
            "object_id": "asdasdjasdqeekqddandadjke",
            "test3": true,
            "test5": "some data"
        }
        let tester = new TestClass()
        tester.import(data)
        expect(tester).toBeDefined()
        expect(tester.isUnique).toBe(false)
        expect(tester.isDirty).toBe(false)
        expect(tester.get("test3")).toBe(true)
        expect(tester.get("test5")).toBe("some data")
        expect(tester.id).toBe("asdasdjasdqeekqddandadjke")
    })

})

describe("Testing CRUD operations of Model", () => {
    const config_CRUD = {
        dev: {
            application: "test", 
            env: Environment,
            infrastructure: {
                authentication: {
                    account: "000000000000",
                    region: "us-east-1",
                    credentials: {
                        key: "some-key",
                        secret: "some-secret"
                    },
                    endpoint: {
                        host: "localhost"
                    }
                }
            },
            security: {
                keys: {
                    vault: {
                        algorithm: "aes-256-cbc",
                        key: "$B&E)H@McQfTjWnZr4u7x!z%C*F-JaNd"
                    }
                }
            }
        }
    }
    class TestClass extends Model {
        public static tableName: string = "testClassTable"

        protected prepare() : void {
            super.prepare()
            this.rules.set("sortie", {type: "text", sortable: true})
            this.rules.set("test1", {type: "text"})
            this.rules.set("test2", {type: "array"})
            this.rules.set("test3", {type: "object"})
        }
    }
    let testObjectId: string    

    test("creating an entry for test class", async () => {
        Application.configure(config_CRUD)
        let tester = new TestClass()
        tester.set("sortie", "some sorting value")
        tester.set("test1", "some test")
        tester.set("test2", ["some", "value", "test"])
        tester.set("test3", {some: "value", value: 1})
        await tester.save()
        testObjectId = tester.id
        expect(tester).toBeDefined()
        expect(tester.isUnique).toBe(false)
        expect(tester.isDirty).toBe(false)
    })

    test("loading up the object", async () => {
        let tester = await TestClass.read(testObjectId)
        expect(tester).toBeDefined()
        expect(tester.isUnique).toBe(false)
        expect(tester.isDirty).toBe(false)
        expect(tester.get("test1")).toBe("some test")
        expect(tester.get("test2")).toBeInstanceOf(Array)
        expect(tester.get("test2")).toHaveLength(3)
        expect(tester.get("test2")[0]).toBe("some")
        expect(tester.get("test2")[1]).toBe("value")
        expect(tester.get("test2")[2]).toBe("test")
        expect(tester.get("test3")).toBeInstanceOf(Object)
        expect(Object.keys(tester.get("test3"))).toHaveLength(2)
        expect(tester.get("test3").some).toBe("value")
        expect(tester.get("test3").value).toBe(1)
    })

    test("updating the entry", async () => {
        let tester = await TestClass.read(testObjectId)
        tester.set("test1", "some new test value")
        await tester.save()
        expect(tester.isUnique).toBe(false)
        expect(tester.isDirty).toBe(false)
        let tester1 = await TestClass.read(testObjectId)
        expect(tester1.get("test1")).toBe("some new test value")
        expect(tester1.get("test2")).toBeInstanceOf(Array)
        expect(tester1.get("test2")).toHaveLength(3)
        expect(tester1.get("test2")[0]).toBe("some")
        expect(tester1.get("test2")[1]).toBe("value")
        expect(tester1.get("test2")[2]).toBe("test")
        expect(tester1.get("test3")).toBeInstanceOf(Object)
        expect(Object.keys(tester1.get("test3"))).toHaveLength(2)
        expect(tester1.get("test3").some).toBe("value")
        expect(tester1.get("test3").value).toBe(1)        
    })

    test("test Model.all()", async () => {
        let testers = await TestClass.all()
        expect(testers).toBeDefined()
        expect(testers).toBeInstanceOf(Array)
        expect(testers.length).toBeGreaterThanOrEqual(1)
    })

    test("test Model.where()", async () => {
        let tester = new TestClass()
        tester.set("sortie", "some sorting value 1")
        tester.set("test1", "some new test value 1")
        await tester.save()
        let testers = await TestClass.where({field: "test1", type: "text", value: "some value"}).get()
        // let filters: Array<IModelFilter> = []
        // filters.push({field: "test1", type: "text", value: "some new test value"})        
        // let testers: Collection = await TestClass.where(filters)->get()
        expect(testers).toBeDefined()
        expect(testers).toBeInstanceOf(Collection)
        expect(testers.count()).toBeGreaterThanOrEqual(1)
        expect(testers.item(0)).toBeInstanceOf(TestClass)
        expect((testers.item(0) as TestClass).get("test1")).toBe("some new test value")        
    })

    test("test Model.delete()", async () => { 
        let responses = await Promise.all((await TestClass.all()).map(async (value: Model) => await value.delete()))
        expect(responses).toBeDefined()
        expect(responses).toBeInstanceOf(Array)
        expect(responses.length).toBeGreaterThanOrEqual(2)        
    })
})

describe("Testing Queue dispatchment", () => {
    class TestClass extends Model {

        public static tableName : string = "testClassTable"

        protected prepare() : void {
            super.prepare()
            this.rules.set("sortie", { type: "text", required: true, sortable: true })
            this.defaults["sortie"] = uuidv4()
        }
    }

    test("Test Model.dispatch()", async () => {
        let tester = new TestClass()
        let response = await tester.dispatch("test-queue", {message: "test"})
        expect(response).toBeDefined()
        expect(response).toBeInstanceOf(Object)
        expect(response.id).toBeDefined()
        // expect(response.sequence).toBeDefined()
    })
})