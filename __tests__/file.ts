import * as fs from 'fs'
import { File, Environment, Application } from '../src'
import { v4 as uuidv4 } from 'uuid'
import IDictionary from '@src/interfaces/dictionary'

describe("Testing File class", () => {

    const config = {
        dev: {
            application: "test", 
            env: Environment,
            infrastructure: {
                authentication: {
                    region: "us-east-1",
                    credentials: {
                        key: "some-key",
                        secret: "some-secret"
                    },
                    endpoint: {
                        host: "localhost"
                    }
                }
            },
            security: {
                keys: {
                    vault: {
                        algorithm: "aes-256-cbc",
                        key: "$B&E)H@McQfTjWnZr4u7x!z%C*F-JaNd"
                    }
                }
            }
        }
    }    

    let storefile: IDictionary
    let filename: string

    test("Create new File", () => {
        Application.configure(config)
        let file = new File()
        expect(file).toBeDefined()
        expect(file).toBeInstanceOf(File)
        expect(file.folder).toHaveLength(0)
        expect(file.filename).toHaveLength(0)
        expect(file.type).toHaveLength(0)
        expect(file.url).toBeUndefined()
    })

    test("Check File.store()", async () => {
        let file = new File()
        let data: Buffer = fs.readFileSync("./test.json")
        filename = uuidv4()
        let result = await file.store("filestore", filename, "application/json", data)
        expect(result).toBeDefined()
        expect(result.folder).toBe("filestore")
        expect(result.filename).toBe(filename)
        expect(result.type).toBe("application/json")
        expect(result.objectId).toBeDefined()
        storefile = result.export()
        expect(storefile).toHaveProperty("folder")
        expect(storefile).toHaveProperty("filename")
        expect(storefile).toHaveProperty("type")
    })

    test("Check File.import()", async () => {
        let file = new File()
        file.import(storefile)
        expect(file.folder).toBe("filestore")
        expect(file.type).toBe("application/json")
        expect(file.filename).toBe(filename)        
    })

    test("Check File.fetch()", async () => {
        let file = new File()
        file.import(storefile)
        let data = await file.fetch()
        let orig: Buffer = fs.readFileSync("./test.json")
        expect(data).toBeDefined()
        expect(data).toBeInstanceOf(Object)
        expect(data.type).toBe(file.type)
        expect((data.data as Buffer).toString('base64')).toBe(orig.toString('base64'))
    })
})