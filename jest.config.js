const { pathsToModuleNameMapper } = require('ts-jest/utils');
// In the following statement, replace `./tsconfig` with the path to your `tsconfig` file
// which contains the path mapping (ie the `compilerOptions.paths` option):
const { compilerOptions } = require('./tsconfig.json');

module.exports = { 
  preset: "ts-jest", 
  testEnvironment: "node", 
  modulePaths: ['<rootDir>'],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
  transform: { // transform files with ts-jest 
    "^.+\\.(js|ts)$": "ts-jest", 
  }, 
  transformIgnorePatterns: [ // allow lit-html transformation 
    "node_modules/(?!\@leungas/*)", 
  ], 
  globals: { 
    "ts-jest": { 
      tsConfig: { // allow js in typescript 
        allowJs: true, 
      }, 
    }, 
  }, 
};