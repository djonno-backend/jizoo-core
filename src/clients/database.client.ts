import {DynamoDB} from 'aws-sdk'
import Application from '../application'
import Model from '../model'
import IAwsClient from '../interfaces/aws-client.setup'
import IDictionary from '../interfaces/dictionary'
import IModelFilter from '../interfaces/model.filter'
import IMetadata from '../interfaces/metadata'
import { Debugger } from '..'

const CFG_AWS_SETUP: string = "infrastructure.authentication"

/**
 * @package @leungas/aws-orm
 * @class DynamoDBClients
 * @description this is the connector to DynamoDB on AWS
 * @author Mark Leung <leungas@gmail.com>
 */
export default class DynamoDBClient {

    private client: DynamoDB                                        /** @var {DynamoDB} client      the AWS DynamoDB client */

    /**
     * creating our instance
     * @param {string} source       our datasource
     */
    constructor () {
        // let setup: IAwsClient = Application.config(`infrastructure.datasources.${source}`)
        const setup: IDictionary = Application.config(CFG_AWS_SETUP)
        let version: string = setup["version"] != undefined ? setup["version"] : "latest"
        const config: DynamoDB.Types.ClientConfiguration = {
            region: (setup as IAwsClient).region,
            apiVersion: version
        }
        if (setup["credentials"] !== undefined) {
            config.credentials = {
                accessKeyId: setup["credentials"].key,
                secretAccessKey: setup["credentials"].secret
            }
        }
        if (setup["endpoint"] !== undefined) {
            let destination = process.env.LOCALSTACK_HOSTNAME ? process.env.LOCALSTACK_HOSTNAME : setup["endpoint"].host
            config.endpoint = process.env.ML_EXT_SYSENV === "local" ? `http://${destination}:4569` : `http://${destination}:4566`
        }
        this.client = new DynamoDB(config)
    }

    /**
     * decoding the dynamodb attribute map into a dictionary 
     * @param {AttributeMap} data   the datasource from Dynamo DB
     * @return {IDictionary}
     */
    private decode(data: DynamoDB.AttributeMap) : IDictionary {
        const result: IDictionary = {}
        Object.keys(data).map((key: string) => {
            const value = data[key]
            switch (Object.keys(value)[0]) {
                case "S": result[key] = value.S; break
                case "SS": result[key] = value.SS; break
                case "N": 
                    let numeric = value.N !== undefined ? value.N : "0.0"
                    result[key] = Number(numeric)
                    break
                case "BOOL": 
                    result[key] = value.BOOL !== undefined ? value.BOOL as boolean :false
                    break
                case "NS": 
                    let dataset = value.NS !== undefined ? value.NS : []
                    result[key] = dataset.map((value: string) => Number(value))
                    break
                case "M": 
                    const tempMap = new Map()
                    let mapValue = value.M !== undefined ? value.M : {}
                    Object.keys(mapValue).map((mapKey: string) => tempMap.set(mapKey, mapValue[mapKey]))
                    result[key] = tempMap
                    break
                case "NULL": result[key] = undefined; break
                case "L": result[key] = value.L !== undefined ? value.L : []; break
            }
        })
        return result
    }

    /**
     * map the value into DynamoDB format
     * @param {string} type         the datatype to ecnode
     * @param {any} value           the value for us to encode
     * @return {AttributeValue}
     */
    private encode(type: string, value: any) : DynamoDB.AttributeValue {
        if (value === undefined || value == null || JSON.stringify(value) === "null") return { NULL: true }
        switch (type) {
            case "array": return { S: JSON.stringify(value) }
            case "date": return { N: (value as Date).getTime().toString() }
            case "file": return { S: JSON.stringify(value.export()) }
            case "decimal": return { N: (<number>value).toString() }
            case "map": return { M: value as IDictionary }
            case "number": return { N: (<number>value).toString() }
            case "numeric-text": return { S: (<number>value).toString() }
            case "object": return { S: JSON.stringify(value) }
            case "text": return { S:  value }
            case "unsigned": return { N: (<number>value).toString() }
            case "yesno": return { BOOL: value }
            default: throw new Error(`Unaccepted typeç ${type} discovered`)
        }
    }

    /**
     * remove the entry from table
     * @param {Model} item          the item to remove
     * @return {any}
     */
    public async delete(item: Model) : Promise<any> {
        let keys: IDictionary = {}
        keys[(<any>item.constructor).tableHashKey] = { S: item.id }
        item.sortables.forEach((value: IMetadata, field: string) => {
            keys[field] = this.encode(value.type, item.get(field))
        })

        let request: DynamoDB.DeleteItemInput = {
            TableName: (<any>item.constructor).table,
            Key: keys
        }
        Debugger.debug(`delete request: ${JSON.stringify(request)}`, this)
        let response = await this.client.deleteItem(request).promise()
        Debugger.debug(`delete response: ${JSON.stringify(response)}`, this)
        return response
    }

    /**
     * getting the item based on key
     * @param {string} table        the table we are reading
     * @param {string} key          the hash key name to read
     * @param {string} id           the value for the key
     * @return 
     */
    public async get(table: string, key: string, id: string) : Promise<any> {
        let request: DynamoDB.QueryInput = {
            TableName: table,
            KeyConditionExpression: `${key} = :object`,
            ExpressionAttributeValues: {
                ":object": {
                    S: id
                }
            }
        }
        let response = await this.client.query(request).promise()
        if (response.Count !== undefined) {
            if (response.Count == 1) {
                if (response.Items !== undefined)
                    return this.decode(response.Items[0])
                else 
                    return undefined
            } else 
                return undefined
        } else 
            return undefined
    }

    /**
     * storing item into DynamoDB as insert
     * @param {Model} item          the item to insert into table
     * @return {any}
     */
    public async put(item: Model) : Promise<any> {
        let values: IDictionary = {}        
        item.values.forEach((attribute: IModelFilter) => {
            values[attribute.field] = this.encode(attribute.type, attribute.value)
        })
        values[(<any>item.constructor).tableHashKey] = {S: item.id}
        let request: DynamoDB.PutItemInput = {
            TableName: (<any>item.constructor).table,
            Item: values
        }
        let response = await this.client.putItem(request).promise()
        return response
    }

    /**
     * doing full or partial scan of a table
     * @param {string} table        the table name we are scanning
     * @param {Array<IModelFilter>} conditions  the filtering conditions
     * @return {any}
     */
    public async scan(table: string, conditions?: Array<IModelFilter>) : Promise<any> {
        let request: DynamoDB.ScanInput = {
            TableName: table
        }
        if (conditions !== undefined && conditions.length > 0) {
            let expression: Array<string> = []
            let names: IDictionary = {}
            let attributes: IDictionary = {} 

            conditions.forEach((value: IModelFilter, index: number) => {
                let operator = value.operator !== undefined ? value.operator : "="
                expression.push(`#f${index} ${operator} :v${index}`)
                names[`#f${index}`] = value.field 
                attributes[`:v${index}`] = this.encode(value.type, value.value)
            })

            request.FilterExpression = expression.join(" AND ")
            request.ExpressionAttributeNames = names
            request.ExpressionAttributeValues = attributes
        }
        let response = await this.client.scan(request).promise()
        if (response.Count !== undefined && response.Count > 0) {
            let result: Array<any> = []
            if (response.Items !== undefined)
                response.Items.forEach((value: DynamoDB.AttributeMap) => result.push(this.decode(value)))
            return result
        } else 
            return []
    }

}