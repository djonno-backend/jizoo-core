import {SNS} from 'aws-sdk'
import { Application, IAwsClient, IDictionary } from '..'

const CFG_AWS_SETUP: string = "infrastructure.authentication"

/**
 * @package @leungas/aws-orm
 * @class BroadcastClient
 * @description this client help us to connect to SNS and send multicast messages
 * @author Mark Leung <leungas@gmail.com>
 */
export default class BroadcastClient {

    private client: SNS                 /** @var {SNS} client           our client to SNS */

    /**
     * initializing our instance
     * @return {BroadcastClient}
     */
    constructor() {
        // let setup: IAwsClient = Application.config(`infrastructure.datasources.${source}`)
        const setup: IDictionary = Application.config(CFG_AWS_SETUP)
        let version: string = setup["version"] != undefined ? setup["version"] : "latest"
        const config: SNS.Types.ClientConfiguration = {
            region: (setup as IAwsClient).region,
            apiVersion: version
        }
        if (setup["credentials"] !== undefined) {
            config.credentials = {
                accessKeyId: setup["credentials"].key,
                secretAccessKey: setup["credentials"].secret
            }
        }
        if (setup["endpoint"] !== undefined) {
            let destination = process.env.LOCALSTACK_HOSTNAME ? process.env.LOCALSTACK_HOSTNAME : setup["endpoint"].host
            config.endpoint = `http://${destination}:4566`
        }
        this.client = new SNS(config)
    }
    
    /**
     * sending out the multicasting message
     * @param {string} topic        the topic we are sending out
     * @param {object} message      the message we are sending
     * @return {boolean}
     */
    public async broadcast(topic: string, message: object) : Promise<boolean> {
        let request: SNS.Types.PublishInput = {
            TopicArn: topic,
            Message: JSON.stringify(message),
            MessageStructure: 'json'
        }
        let response = await this.client.publish(request).promise()        
        return response.MessageId !== undefined
    }
}