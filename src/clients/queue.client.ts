import {SQS} from 'aws-sdk'
import Application from '../application'
import QueueTransactionException  from '../exceptions/queue.transaction'
import IAwsClient from '../interfaces/aws-client.setup'

const CFG_AWS_SETUP: string = "infrastructure.authentication"       /** @const {string} CFS_AWS_SETUP   config path for AWS auth schema */
const CFG_ENV_STAGE: string = "env.stage"                           /** @const {string} CFG_ENV_STAGE   config path for stage info */

/**
 * @package @leungas/aws-orm
 * @class SqsClient
 * @description this is a client to connect to SQS for decoupling async message dispatchment
 * @author Mark Leung <leungas@gmail.com>
 */
export default class SqsClient {

    private client: SQS                             /** @var {SQS} client       the AWS SQS Client */

    /**
     * creating our SQS client
     * @return {void}
     */
    constructor () {
        let setup = Application.config(CFG_AWS_SETUP) as IAwsClient
        let config: SQS.Types.ClientConfiguration = {
            region: setup.region
        }
        if (setup.credentials !== undefined) 
            config.credentials = {
                accessKeyId: setup.credentials.key,
                secretAccessKey: setup.credentials.secret
            }
        if (setup.endpoint !== undefined) {
            let destination = process.env.LOCALSTACK_HOSTNAME ? process.env.LOCALSTACK_HOSTNAME : setup["endpoint"].host
            config.endpoint = process.env.ML_EXT_SYSENV === "local" ? `http://${destination}:4576` : `http://${destination}:4566`
        }
        
        this.client = new SQS(config)
    }

    /**
     * delete the message off from queue
     * @param {string} queue        the queue the message is 
     * @param {string} message      the message handle id
     * @return {any}
     */
    public async delete(queue: string, message: string, override?: boolean) : Promise<any> {
        let request: SQS.Types.DeleteMessageRequest = {
            QueueUrl: this.getQueueAddress(queue, override),
            ReceiptHandle: message
        }
        let response = await this.client.deleteMessage(request).promise()
        return response
    }

    /**
     * get the queue URL 
     * @param {string} queue        the queue name we want to connect
     * @return {string} 
     */
    private getQueueAddress(queue: string, override?: boolean) : string {
        override = override
        let setup = Application.config(CFG_AWS_SETUP) as IAwsClient
        let stage = Application.config(CFG_ENV_STAGE)
        let account = stage === "local" ? "queue" : setup.account
        let result = process.env.ML_EXT_SYSENV === "local" ? `${this.client.endpoint.href}queue/${queue}` : `${this.client.endpoint.href}${account}/${queue}`
        console.log(`queue-url: ${result}`)
        return result
    }

    /**
     * submitting to a queue
     * @param {any} message         the message to deposite
     * @return {any}
     */
    public async put(queue: string, message: object, override?: boolean) : Promise<any> {
        let request: SQS.Types.SendMessageRequest = {
            QueueUrl: this.getQueueAddress(queue, override),
            MessageBody: JSON.stringify(message)
        }
        let response = await this.client.sendMessage(request).promise()
        if (response.MessageId !== undefined) {
            return {
                id: response.MessageId,
                sequence: response.SequenceNumber
            }
        } else 
            throw new QueueTransactionException(queue, message)
    }
}