import Application from "../application";
import IAwsClient from "../interfaces/aws-client.setup";
import { Lambda } from "aws-sdk";

const CFG_AWS_SETUP: string = "infrastructure.authentication"

/**
 * @package @leungas/aws-orm
 * @class LambdaClient
 * @description the client to connect lambda for function to function call
 * @author Mark Leung <leungas@gmail.com>
 */
export default class LambdaClient {

    private client: Lambda

    /**
     * create the instance
     * @return {void}
     */
    constructor () {
        let setup: IAwsClient = Application.config(CFG_AWS_SETUP)
        let config: Lambda.Types.ClientConfiguration = {
            region: setup.region
        }
        if (setup.credentials !== undefined) 
            config.credentials = {
                accessKeyId: setup.credentials.key,
                secretAccessKey: setup.credentials.secret
            }
        if (setup.endpoint !== undefined) {
            let port = setup.endpoint.port.lambda !== undefined ? setup.endpoint.port.lambda : 4566
            let destination = process.env.LOCALSTACK_HOSTNAME ? process.env.LOCALSTACK_HOSTNAME : setup["endpoint"].host
            config.endpoint = `http://${destination}:${port}`
        }
            
        this.client = new Lambda(config)
    }

    /**
     * executing function in sync mode
     * @param {string} call         the function call to run
     * @param {object} message      the message to trigger
     * @param {string} service      the service we are targeting
     * @return {any}
     */
    public async run(call: string, message: object, service?: string) : Promise<any> {
        let request: Lambda.Types.InvocationRequest = {
            FunctionName: Application.config("application") + "-" + (service ? service : Application.config("service")) + "-" + Application.config("env.stage") + "-" + call,
            InvocationType: "RequestResponse",
            Payload: JSON.stringify(message)
        }
        let response = await this.client.invoke(request).promise()
        if (response.Payload !== undefined) {
            if (typeof response.Payload === "string") {
                return JSON.parse(response.Payload)
            } else {
                return response.Payload
            }
        } else 
            return undefined        
    }

}