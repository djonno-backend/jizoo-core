import { S3 } from "aws-sdk"
import IAwsClient from "../interfaces/aws-client.setup"
import Application from "../application"

const CFG_AWS_SETUP: string = "infrastructure.authentication"

export default class S3Client {

    private client: S3                          /** @var {S3} client        the client applicable for S3 */
    private bucket: string                      /** @var {string} bucket    the bucket name for connection */
    
    /**
     * initialize the instnace
     * @param {string} bucket       the bucket name
     * @return {void} 
     */
    constructor (bucket: string) {
        let setup: IAwsClient = Application.config(CFG_AWS_SETUP)
        let config: S3.Types.ClientConfiguration = {
            region: setup.region,
            s3ForcePathStyle: true
        }
        if (setup.credentials !== undefined) 
            config.credentials = {
                accessKeyId: setup.credentials.key,
                secretAccessKey: setup.credentials.secret
            }
        if (setup.endpoint !== undefined) {
            let destination = process.env.LOCALSTACK_HOSTNAME ? process.env.LOCALSTACK_HOSTNAME : setup["endpoint"].host
            config.endpoint = `http://${destination}:4566`
        }
        this.client = new S3(config)
        this.bucket = bucket
    }

    public async get(filename: string) : Promise<any> {
        let request: S3.Types.GetObjectRequest = {
            Bucket: this.bucket,
            Key: filename
        }
        let response = await this.client.getObject(request).promise()        
        if (response.Body !== undefined) {        
            return response.Body instanceof Buffer ? response.Body : Buffer.from(response.Body)
        } else 
            return undefined
    }

    /**
     * store a file onto S3
     * @param {string} filename     the name of the file (with path)
     * @param {string} type         the data type
     * @param {Buffer} data         the data buffer
     * @return {any}
     */
    public async put(filename: string, type: string, data: Buffer) : Promise<any> {
        let request: S3.Types.PutObjectRequest = {
            Bucket: this.bucket,
            ContentType: type,
            Key: filename,
            Body: data
        }
        let response = await this.client.putObject(request).promise()
        if (response.ETag !== undefined) {
            return {
                id: response.ETag
            }
        }
    }
}