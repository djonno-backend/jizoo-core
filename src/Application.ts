import IDictionary from "./interfaces/dictionary"

export default abstract class Application {

    private static configuration: IDictionary = {}

    /**
     * setting the configuration for the application
     * @param {object} source       our configuration source
     * @return {void}
     */
    public static configure(source: object) : void {
        Application.configuration = source
    }

    /**
     * getting the parameter within the configuration
     * @param {string} param        the parameter we are reading
     * @param {IDictionary?} source      the source object we are reading from
     * @return {any}
     */
    public static config(param: string, source?: IDictionary) : any {
        const query = param.split(".")
        const datasource = source !== undefined ? source : Application.configuration
        const stage = process.env.ML_EXT_SYSENV !== undefined ? process.env.ML_EXT_SYSENV : "dev"
        let result: any
        if (source === undefined) query.unshift(stage)        
        query.forEach((item: string, index: number) => {            
            const value = datasource[item]
            if (value !== undefined) {
                if (typeof value !== "object") {
                    if (index < query.length - 1)
                        result = undefined 
                    else 
                        result = value
                    return false                        
                } else {
                    const subquery = query.slice(index + 1)                     
                    if (subquery.length > 0) {
                        result = Application.config(subquery.join("."), value)
                    } else 
                        result = value
                }                        
            } else 
                return false
        })        
        return result
    }

}