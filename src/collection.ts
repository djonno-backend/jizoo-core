/**
 * @package leungas/aws-orm
 * @class Collection
 * @description the Collection class is to help us to handle object array from Query
 * @author Mark Leung <leungas@gmail.com>
 */
export default class Collection {

    private objects: object[] = [];         /** @var {object[]} objects     the stored object within this instance */

    /**
     * initializing our instance
     * @param {object[]?} data      our initial data
     * @return {Collection}
     */
    constructor(data?: object[]) {
        this.objects = data ?? []
    }

    /**
     * return all the values in collection
     * @return {object[]}
     */
    public all() : object[] {
        return this.objects
    }

    /**
     * adding new data onto the object
     * @return {Collection}
     */
    public add(data: object[]) : Collection {
        this.objects = [...this.objects, ...data]
        return this
    }

    /**
     * get the number of elements inside the collection
     * @return {number}
     */
    public count() : number {
        return this.objects.length
    }

    /**
     * get only very first entry in the collection
     * @return {object|undefined}
     */
    public first() : any {
        return this.objects.length > 0 ? this.objects[0] : undefined
    }

    /**
     * filter the object list based on a callback method
     * @param {fn} callback         the function to trigger the filtering test
     * @return {object[]}
     */
    public filter(callback: (item: object, index: number, array: object[]) => item is object) : object[] {
        return this.objects.filter(callback)
    }

    /**
     * get the object at a specific index
     * @param {number} index        the index to get within the collection data
     * @return {object}
     */
    public item(index: number) {
        return index < this.objects.length ? this.objects[index] : undefined
    }

    /**
     * get the very last item in the collect array
     * @return {object|undefined}
     */
    public last() : any {
        return this.objects.length > 0 ? this.objects[this.objects.length - 1] : undefined
    }

    /**
     * removing instances based on the filtering condition
     * @param callback              the function to remove the data
     * @return Collection
     */
    public remove(callback: (item:object) => boolean) : Collection {
        let result: object[] = []
        this.objects.forEach((item) => {
            if (callback(item) === false) 
                result.push(item) 
        })
        this.objects = result        
        return this
    }

}