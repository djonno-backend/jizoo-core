export default interface IDictionary {
    [name: string]: any
}