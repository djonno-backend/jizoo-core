export default interface IMetadata {
    after?: Date,
    before?: Date,
    not_empty?: boolean,
    encrypted?: boolean,
    length?: number,
    limits?: string[],
    limitIf?: {
        field: string,
        when: string,
        limits: string[]
    },
    max?: number,
    maxLength?: number,
    min?: number,
    minLength?: number,
    omitIf?: string,
    required?: boolean,
    requiredIf?: string,
    sortable?: boolean,
    type: "array" | "date" | "decimal" | "file" | "map" | "number" | "numeric-text" | "object" | "text" | "unsigned" | "yesno"
}