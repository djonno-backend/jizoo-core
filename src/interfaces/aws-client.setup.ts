export default interface IAwsClient {
    account: string,
    region: string,
    version?: string,
    credentials?: {
        key: string,
        secret: string
    },
    endpoint?: {
        host: string,
        port: {
            dynamodb?: number,
            lambda?: number,
            sqs?: number,
            s3?: number
        }
    }
}