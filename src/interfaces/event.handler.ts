export default interface IEventHandler {
    on(handler: { (data?: any) : void }) : void
    off(handler: { (data?: any) : void }) : void
}