export default interface IModelFilter {
    field: string,
    type: "array" | "date" | "decimal" | "file" | "map" | "number" | "numeric-text" | "object" | "text" | "unsigned" | "yesno",
    value: any,
    operator?: string
}