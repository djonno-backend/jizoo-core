import { v4 as uuidv4 } from 'uuid'
import * as crypto from 'crypto'
import DynamoDBClient from './clients/database.client'
import IModelFilter from "./interfaces/model.filter";
import IDictionary from "./interfaces/dictionary";
import IMetadata from "./interfaces/metadata";
import BroadcastClient from './clients/broadcast.client'
import ModelNotFoundException from './exceptions/model.not-found'
import ValueNotAcceptedException from './exceptions/model.value.not-accepted'
import FieldNotDefinedException from "./exceptions/model.field.not-accepted";
import UnknownMetadataTypeException from "./exceptions/model.metata.unknown";
import Application from './application';
import EncryptionBadConfigException from './exceptions/encryption.bad-require';
import UncipherDataException from './exceptions/encryption.bad-data';
import SqsClient from './clients/queue.client';
import Debugger from './helpers/debugger';
import {EventEmitter} from 'events'
import LambdaClient from './clients/function.client';
import File from './file'
import Query from './query';

const CFG_CRYPTO_ALGO = "security.keys.vault.algorithm"         /** @const {string} CFG_CRYPTO_ALGO     the algorithm for data encryption  */
const CFG_CRYPTO_KEY = "security.keys.vault.key"                /** @const {string} CFG_CRYPTO_KEY      the key for encryption */
const CFG_INFRA_QUEUES = "infrastructure.loopback.queues"       /** @const {string} CFG_INFRA_QUEUES    the loopback controller per queue  */

/**
 * @package @leungas/aws-orm
 * @class Model
 * @description the ORM model we can use to do system objects
 * @author Mark Leung <leungas@gmail.com>
 */
export default class Model {

    private dirty: boolean = false                              /** @var {boolean}          dirty       the state of the instance */
    private objectId: string = String()                         /** @var {string}           objectId    the id of the instance */
    private unique: boolean = true                              /** @var {boolean}          unique      whether the item is brand new */
    private deleted: boolean = false                            /** @var {boolean}          deleted     whether the item is deleted */

    protected data: IDictionary = {}                            /** @var {IDictionary}      data        our data storage */
    protected defaults: IDictionary = {}                        /** @var {IDictionary}      defaults    the default dataset for the fields */
    protected masks: Array<string> = []                         /** @var {Array<string>}    masks       the list of fields to hide in export */
    protected rules: Map<string, IMetadata> = new Map()         /** @var {Map<string, IMetadata>} rules the set of metadata control over fields */

    public static tableHashKey: string = "object_id"            /** @var {string}       tableHashKey    the hash key name for the table */
    public static tableName: string                             /** @var {string}       tableName       the name of the table */
    public static tableSource: string = "default"               /** @var {string}       tableSource     the datasource ref in config for system */
    protected static namespace: any = undefined                 /** @var {any}          namespace       the class module we need to import for querying */

    protected afterMount: EventEmitter = new EventEmitter()
    protected afterSave: EventEmitter = new EventEmitter()
    protected beforeDelete: EventEmitter = new EventEmitter()

    /**
     * creating our instance
     * @return {void}
     */
    constructor () {
        this.objectId = uuidv4()
        this.prepare()
        Object.keys(this.defaults).map((value: string) => this.set(value, this.defaults[value]))
    }

    /**
     * check if we need to encrypt the data in the field
     * @param {string} field                    the field to set
     * @param {any} value                       the value to set to field
     * @return {any}
     */
    private cipher(field: string, value: any) : any {
        let ruleset = this.rules.get(field)
        if (ruleset !== undefined && ruleset.encrypted !== undefined && ruleset.encrypted === true) {
            let key = Application.config(CFG_CRYPTO_KEY)
            let algorithm = Application.config(CFG_CRYPTO_ALGO)
            if (key && algorithm) {
                let nounce = crypto.randomBytes(16)
                let cipher = crypto.createCipheriv(algorithm, Buffer.from(key, 'utf-8'), nounce)
                let cipherText = cipher.update(value)
                let result = Buffer.concat([cipherText, cipher.final()])
                return nounce.toString('hex') + "::" + result.toString('hex')
            } else 
                throw new EncryptionBadConfigException()
        } else 
            return value
    }

    /**
     * check if the field is defined in the metadata for access
     * @param {string} field                    the field we try to read
     * @return {boolean}
     */
    private isFieldValid(field: string) : boolean {
        return this.rules.get(field) !== undefined
    }

    /**
     * check if value to set is valid for a specific field
     * @param {string} field                    the field to set
     * @param {any} value                       the value to set to field
     * @return {boolean}
     */
    private isValueValid(field: string, value: any) : boolean {
        let ruleset = this.rules.get(field)
        if (ruleset != undefined) {
            if (ruleset.required !== undefined && ruleset.required === true && value === undefined) {
                return false
            } else if (ruleset.requiredIf !== undefined) {

            } else if (ruleset.omitIf !== undefined) {

            } 
            
            let canAfter: boolean = false 
            let canBefore: boolean = true
            let canNotEmpty: boolean = false
            let canLength: boolean = false
            let canLimit: boolean = false
            // let canLimitIf: boolean = false
            let canMax: boolean = false 
            let canMin: boolean = false
            let canMinLength: boolean = false 
            let canMaxLength: boolean = false 

            // if our value is some value and not null
            if (value !== undefined && value !== null) {
                switch (ruleset.type) {
                    case "array":
                        if (!(value instanceof Array)) return false
                        canMinLength = true
                        canMaxLength = true
                        canNotEmpty = true
                        break
                    case "date":
                        if (!(value instanceof Date)) {
                            return false 
                        }
                        canAfter = true
                        canBefore = true
                        break
                    case "decimal":
                        if (typeof value !== "number") 
                            return false 
                        else 
                            if (value % 1 !== 0) return false
                        canMin = true
                        canMax = true
                        break
                    case "file":
                        if (!(value instanceof File)) return false
                        break
                    case "map": 
                        if (!(value instanceof Map)) return false 
                        break
                    case "number": 
                        if (typeof value !== "number") return false
                        canMin = true
                        canMax = true                    
                        break
                    case "numeric-text":
                        if (!Number.parseInt(value)) return false 
                        canMin = true
                        canMax = true                    
                        break
                    case "object":
                        if (!(value instanceof Object)) return false
                        break
                    case "text":
                        if (typeof value !== "string") return false
                        canLength = true
                        canLimit = true
                        // canLimitIf = true
                        canMinLength = true
                        canMaxLength = true        
                        break
                    case "unsigned":
                        if (typeof value !== "number") 
                            return false 
                        else 
                            if (value < 0) return false 
                        canMin = true
                        canMax = true                           
                        break
                    case "yesno":
                        if (typeof value !== "boolean") return false 
                        break
                    default:
                        throw new UnknownMetadataTypeException(field, ruleset.type)
                }

                if (canAfter === true) 
                    if (ruleset.after !== undefined) 
                        if ((value as Date).getUTCMilliseconds() < (new Date(ruleset.after)).getUTCMilliseconds())
                            return false
                if (canBefore === true)
                    if (ruleset.before !== undefined)
                        if ((new Date(value)).getUTCMilliseconds() > (new Date(ruleset.before)).getUTCMilliseconds()) 
                            return false
                if (canNotEmpty === true)
                    if (ruleset.not_empty !== undefined && value.length == 0) 
                        return false
                if (canLength === true)
                    if (ruleset.length !== undefined  && value.length != ruleset.length)
                        return false 
                if (canLimit === true)
                    if (ruleset.limits != undefined && !ruleset.limits.includes(value)) 
                        return false
                if (canMax === true) 
                    if (ruleset.max !== undefined && ruleset.max < value)
                        return false
                if (canMin === true) 
                    if (ruleset.min !== undefined && ruleset.min > value)
                        return false
                if (canMinLength === true)
                    if (ruleset.minLength !== undefined && ruleset.minLength > value.length)
                        return false
                if (canMaxLength === true)
                    if (ruleset.maxLength !== undefined && ruleset.maxLength < value.length)
                        return false
            }

            return true
        } else 
            throw new FieldNotDefinedException(field)
    }

    /**
     * decipher the value if we have encryption on data applied
     * @param {string} field                    the field to check
     * @param {any} value                       the value we are to decipher
     * @return {any}
     */
    private uncipher(field: string, value: any) : any {
        let ruleset = this.rules.get(field)
        if (ruleset !== undefined && ruleset.encrypted !== undefined && ruleset.encrypted === true) {
            let cipherText = (value as String).split("::")
            if (cipherText.length == 2) {
                let algorithm = Application.config(CFG_CRYPTO_ALGO)
                let key = Application.config(CFG_CRYPTO_KEY)
                if (algorithm && key) {
                    let nounce = Buffer.from(cipherText[0], 'hex')
                    let data = Buffer.from(cipherText[1], 'hex')    
                    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key, 'utf-8'), nounce)
                    let restoredText = decipher.update(data)
                    let result = Buffer.concat([restoredText, decipher.final()])
                    return result.toString()
                } else 
                    throw new EncryptionBadConfigException()
            } else 
                throw new UncipherDataException(field)
        } else 
            return value
    }

    /**
     * calling another service function on lambda for data
     * @param {string} call                     the call to execute (function name)
     * @param {object} message                  the message we are sending (payload)
     * @param {string} service                  the name of the service to trigger the call
     * @return {any}
     */
    protected async ask(call: string, message: object, service?: string) : Promise<any> {
        let client = new LambdaClient()
        return await client.run(call, message, service)
    }

    /**
     * sending out multicast message
     * @param {string} topic                    the topic on SNS we are sending to
     * @param {object} message                  the message we want to send out
     * @return {boolean}
     */
    protected async push(topic: string, message: object) : Promise<boolean> {
        let client = new BroadcastClient()
        return client.broadcast(topic, message)
    }

    /**
     * getting name of an object and return 
     * @param {T} object                        the type of the object
     * @return {any}
     */
    protected static getClassName(object: Model) : any {
        return object.constructor.name
    }

    /**
     * this is our initial setup for the instance
     * @return {void}
     */
    protected prepare() : void {
        this.rules.set("created", {type: "date", required: true})               /** @var {Date} created     the date when things are created */
        this.rules.set("updated", {type: "date"})                               /** @var {Date} updated     the date when things are changed */
        this.defaults["created"] = new Date()
        this.afterMount = new EventEmitter()
        this.afterSave = new EventEmitter()
        this.beforeDelete = new EventEmitter()
    }

    /**
     * this is the function we do before saving 
     * @return {void}
     */
    protected async presave() : Promise<void> {}

    /**
     * attribute id: the id of the object
     * @return {string}
     */
    public get id() : string {
        return this.objectId
    }

    /**
     * attribute isDeleted: whether the object is deleted
     * @return {boolean}
     */
    public get isDeleted() : boolean {
        return this.deleted
    }

    /**
     * attribute isDirty: get the dirty status
     * @return {boolean}
     */
    public get isDirty() : boolean {
        return this.dirty
    }

    /**
     * attribute isUnqiue: get the unique status
     * @return {boolean}
     */
    public get isUnique() : boolean {
        return this.unique
    }

    /**
     * attribute sortables: get the values that needs to appear on keys
     * @return {Map<string, IMetadata>}
     */
    public get sortables() : Map<string, IMetadata> {
        let result: Map<string, IMetadata> = new Map()
        this.rules.forEach((value: IMetadata, key: string) => {
            if (value.sortable !== undefined && value.sortable === true)
                result.set(key, value)
        })
        return result
    }
    
    /**
     * attribute table: get the full table name for DynamoDB
     * @return {string}
     */
    public static get table() : string {
        return Application.config("application") + "." + Application.config("env.stage") + "." + this.tableName
    }

    /**
     * exporting the value as a filter set
     * @return {Array<IModelFilter>}
     */
    public get values() : Array<IModelFilter> {
        let result: Array<IModelFilter> = []
        this.rules.forEach((value: IMetadata, field: string) => {
            if (this.get(field) !== undefined)
                result.push({ field: field, type: value.type, value: this.get(field) })
        })
        return result
    }

    /**
     * get all entries of this model
     * @return {Array<Model>}
     */
    public static async all() : Promise<Array<Model>> { 
        let result: Array<Model> = []
        let client = new DynamoDBClient()
        let data: Array<IDictionary> = await client.scan(this.table)
        data.forEach((value: IDictionary) => result.push((new this).import(value)))
        return result
    }

    /**
     * remove the instance 
     * @return {void}
     */
    public async delete() : Promise<any> {
        this.beforeDelete.emit("onDelete", this)
        let client = new DynamoDBClient()
        let response = await client.delete(this)
        this.deleted = true
        this.unique = true
        this.dirty = true
        return response
    }

    /**
     * dispatch message to the queue
     * @param {string} queue                    the queue we want to send message to
     * @param {object} message                  the message we want to send
     * @param {string} service                  the service where the queue belongs to
     * @return {any}
     */
    public async dispatch(queue: string, message: object, service?: string) : Promise<any> {
        const loopbacker = CFG_INFRA_QUEUES + "." + queue
        if (Debugger.loopback === false || Debugger.loopback === true && Application.config(loopbacker)) {
            let client: SqsClient = new SqsClient()
            let queueName: string = `${Application.config("application")}-${(service ? service : Application.config("service"))}-${Application.config("env.stage")}-${queue}`                
            return await client.put(queueName, message)
        } else 
            Debugger.debug(`Queue ${queue} with message: ${JSON.stringify(message)}`, this)
    }    

    /**
     * exporting the data of the instnace
     * @param {IDictionary} options             the option mode for the export
     * @return {IDictionary}
     */
    public async export(options?: IDictionary) : Promise<IDictionary> {
        let result: IDictionary = {}
        result[(<any>this.constructor).tableHashKey] = this.id
        this.rules.forEach((value: IMetadata, field: string) => {
            value = value
            if (!this.masks.includes(field)) {
                result[field] = this.get(field)
            }
        })
        switch (options) {}
        return result
    }

    /**
     * get the model value in transient
     * @param {string} field                    the field to load
     * @return {any}
     */
    public get(field: string) : any { 
        if (this.isFieldValid(field) === true) {
            let value = this.uncipher(field, this.data[field])
            return value
        } else 
            throw new FieldNotDefinedException(field)
    }    

    /**
     * importing data into the instance during load
     * @param {IDictionary} data                the dataset to load
     * @return {Model}
     */
    public import(data: IDictionary) : Model {
        Object.keys(data).map((field: string) => {
            if (field == (<any>this.constructor).tableHashKey) {
                this.objectId = data[field]
            } else {
                if (this.rules.get(field) !== undefined) {                    
                    const type = this.rules.get(field)?.type
                    let value: any
                    switch (type) {
                        case "array": 
                        case "object": 
                            value = JSON.parse(data[field]); 
                            break
                        case "date":
                            value = new Date(data[field])
                            break
                        case "file": 
                            value = new File()
                            value.import(JSON.parse(data[field]))
                            break
                        case "map": 
                            value = (JSON.parse(data[field])) as Map<string,any>
                            break
                        default: value = data[field]
                    }
                    this.data[field] = value
                } else 
                    throw new UnknownMetadataTypeException(field, "")
            }
        })
        this.afterMount.emit("onMount", this)
        this.unique = false 
        this.dirty = false
        return this
    }

    /**
     * loading a model from its id
     * @param {string} id                       the ID of the instance to load
     * @return {Model}
     */
    public static async read(id: string) : Promise<Model> {      
        let client = new DynamoDBClient()
        let data = await client.get(this.table, this.tableHashKey, id)
        if (data !== undefined) {
            let result = new this()
            return result.import(data)            
        } else 
            throw new ModelNotFoundException((<any>this.constructor).name, id)
    }

    /**
     * saving the model into persistance
     * @param {Map<string, any>} options        the options applicable to save
     * @return {Model}
     */
    public async save(options?: Map<string, any>) : Promise<Model> {
        options = options
        if (this.isDirty === true) {
            let client = new DynamoDBClient()
            if (this.isUnique === true) await this.presave()
            await client.put(this)            

            // trigger post save
            this.afterSave.emit("onSave", this)

            if (this.unique === true) this.unique = false 
            this.dirty = false 
            return this
        }
        return this   
    }

    /**
     * define data into model transience
     * @param {string} field                    the field to define 
     * @param {any} value                       the value to define
     * @return {Model}
     */
    public set(field: string, value: any) : Model {
        if (this.isValueValid(field, value) === true) {
            this.data[field] = this.cipher(field, value)
            this.dirty = true 
            return this
        } else 
            throw new ValueNotAcceptedException(field)        
    }

    /**
     * generate a query for select objects on the fly
     * @param {IModuleFilter} filter            our first query filter. once the query is returned all will add to query
     * @return {Query} 
     */
    public static where(filter: IModelFilter) : Query {
        if (this.namespace !== undefined) {
            let result = new Query(this.namespace, this.constructor.name, this.table)
            result.where(filter)
            return result
        } else 
            throw new Error(`This model does not allow object querying. Make sure namespace is configured`)
    }

    /**
     * get selective items of the Model
     * @param {Array<IModelFilter>} filters     the filters to apply in search
     * @return {Array<Model>}
     */
    // public static async where(filters: Array<IModelFilter>) : Promise<Array<Model>> {
    //     let client = new DynamoDBClient()
    //     let data = await client.scan(this.table, filters) as Array<IDictionary>
    //     let result: Array<Model> = []
    //     data.forEach((value: IDictionary) => {
    //         let item = new this()
    //         item.import(value)
    //         result.push(item)
    //     })
    //     return result
    // }
}