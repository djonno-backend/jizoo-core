export const env = {
    debug: process.env.ML_EXT_DEBUG,
    loopback: process.env.ML_EXT_LOOPBACK,
    region: process.env.ML_CLOUD_REGION,
    stage: process.env.ML_EXT_SYSENV,
    token: "X-Application-Token",
    scopeToken: "X-Application-Scope"
}

export const env_prod = {
    debug: false,
    loopback: false,
    region: process.env.ML_CLOUD_REGION,
    stage: process.env.ML_EXT_SYSENV,
    token: "X-Application-Token",
    scopeToken: "X-Application-Scope"
}