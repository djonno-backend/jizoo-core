import Application from "../application";

const CFG_ENV_DEBUG = "env.debug"
const CFG_ENV_LOOPBACK = "env.loopback"

export default abstract class Debugger {

    /**
     * get the loopback flag
     * @return {boolean}
     */
    public static get loopback() : boolean {
        return this.on === true && Application.config(CFG_ENV_LOOPBACK) === "YES"
    }

    /**
     * get the debug flag of the application
     * @return {boolean}
     */
    public static get on() : boolean {     
        let debug = Application.config(CFG_ENV_DEBUG)
        console.log(`DEBUG: ${JSON.stringify(debug)}`)
        return debug === "YES"
    }

    /**
     * logging debugger message onto console
     * @param {string} message      the message to log
     * @param {Model} source        the model to log
     * @return {void}
     */
    public static debug(message: string, source: Object) : void {      
        if (this.on === true) {            
            let instance = source === undefined ? "<anonymous>" : source.constructor.name            
            console.log(`[${(new Date).toString()}] ${instance}: ${message}`)
        }
    }

}