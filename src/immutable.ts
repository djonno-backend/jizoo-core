import Model from "./model";
import IDictionary from "./interfaces/dictionary";
import ModelNotFoundException from "./exceptions/model.not-found";
import InvalidImmutableTraceException from "./exceptions/immutable.bad-trace";

/**
 * @package @leungas/aws-orm
 * @class Immutable
 * @description this class deal with objects that are not "detetable" and keep versions of itself
 * @author Mark Leung <leungas@gmail.com>
 */
export default class Immutable extends Model {

    private backup: IDictionary= {}                 /** @var {IDictionary} backup   our data pre-edit state */

    /**
     * we clone the data of the entry with a brand new instance id
     * @return {any}
     */
    private async clone() : Promise<any> {
        let result = new (<any>this.constructor)() 
        Object.keys(this.backup).map((value: string) => result.set(value, this.backup[value]))
        await result.save()
        return result
    }

    /**
     * override the standard prepare function
     * @return {void}
     */
    protected prepare() : void {
        super.prepare()
        this.rules.set("version", {type: "number", required: true, min: 0})
        this.rules.set("parent", {type: "text"})
        this.rules.set("removed", {type: "yesno", required: true})
        this.defaults.set("deleted", false)
        this.defaults.set("version", 1)        
    }

    /**
     * return the deleted state of the instance
     * @return {boolean}
     */
    public get removed() : boolean {
        return this.get("removed")
    }

    /**
     * this is an override of the delete function, object does not get deleted, but a new flag
     * @return {void}
     */
    public async delete() : Promise<void> {
        this.beforeDelete.emit("onDelete")
        this.set("removed", true)
        await this.save()
    }

    /**
     * this is to override the reading of object to nullify deleted data
     * @param {string} id                   the object id to retrieve
     * @return {Model} 
     * 
     */
    public static async read(id: string) : Promise<Model> {
        let result = await super.read(id) as Immutable
        if (result.removed === true) 
            throw new ModelNotFoundException(this.constructor.name, id)
        return result
    }

    /**
     * our override save function to take care of object clone before save
     * @param {Map<string, any>} options    the options for save (if we need)
     * @return {Model}
     */
    public async save(options?: Map<string, any>) : Promise<Model> {
        if (this.isDirty === true) {
            if (this.isUnique === false) {
                let cloner = await this.clone()
                this.set("parent", cloner.id)
                this.set("version", (this.get("version") as number) + 1)
                await super.save(options)
            } else 
                await super.save(options)
        }
        return this
        
    }

    /**
     * override the origiunal set in order for us to safe keep original data state
     * @param {string} field                    the field to define 
     * @param {any} value                       the value to define
     * @return {Model}
     */
    public set(field: string, value: any) : Model {
        if (this.isDirty === false)                                      //  only do it once when the first undirtied state
            this.backup = this.data                                     //      duplicate the old data    
        return super.set(field, value)                                  //  now run the set
    }   

    /**
     * trace back our data to see old states
     * @param {number} generations          the copies to look back (default to 1), currently support up to 5 generations
     * @return {Immutable}
     */
    public async trace(generations: number = 1) : Promise<Array<Immutable>> {
        let parent = this.get("parent")
        if (parent !== undefined) {
            if (generations > 5) {
                let result = []
                for (let i = 0; i < generations; i++) {
                    if (parent !== undefined) {
                        let item = await (<any>this.constructor).read(parent)
                        result.push(item)
                        parent = item.get("parent")    
                    } else 
                        break
                }
                return result    
            } else 
                throw new InvalidImmutableTraceException(generations)
        } else
            return []
    }
}