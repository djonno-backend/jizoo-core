export default class EncryptionBadConfigException extends Error {
    constructor () {
        super("Encryption settings not properly defined.")
    }
}