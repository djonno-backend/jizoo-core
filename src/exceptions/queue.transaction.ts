export default class QueueTransactionException extends Error {
    constructor (queue: string, message: object) {
        super(`Unable to send message to ${queue} with payload: ${JSON.stringify(message)}`)
    }
}