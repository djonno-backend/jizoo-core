export default class ModelNotFoundException extends Error {
    constructor(instance: string, id: string) {
        super(`Cannot load instance ${instance} with id ${id}`)
    }
}