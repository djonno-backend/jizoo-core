export default class InvalidImmutableTraceException extends Error {
    constructor (generations: number) {
        super(`Cannot complete trace since generations too big. Expect < 10, but received ${generations}`)
    }
}