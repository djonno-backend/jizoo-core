export default class StorageFiledException extends Error {
    constructor (bucket: string, file: string) {
        super(`Unable to store file ${file} on ${bucket} in S3.`)
    }
}