export default class ValueNotAcceptedException extends Error {
    constructor (field: string) {
        super(`Trying to set ${field} with an invalid value`)
    }
}