export default class FieldNotDefinedException extends Error {
    constructor (field: string) {
        super(`Trying to read ${field} but is not allowed`)
    }
}