export default class UncipherDataException extends Error {
    constructor (field: string) {
        super(`Unable to decrypt data for field ${field}`)
    }
}