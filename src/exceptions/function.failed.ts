export default class LambdaExecutionException extends Error {
    constructor (code: number, message: any) {
        super(`Execution failed with code ${code}. Message: ${message}`)
    }
}