export default class UnknownMetadataTypeException extends Error {
    constructor (field: string, type: string) {
        super(`Unknown data type ${type} found in field ${field}`)
    }
}