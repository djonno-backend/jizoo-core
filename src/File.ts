import Application from "./application"
import IAwsClient from "./interfaces/aws-client.setup"
import S3Client from "./clients/storage.client"
import IDictionary from "./interfaces/dictionary"
import StorageFiledException from './exceptions/storage.failed'

const CFG_AWS_SETUP: string = "infrastructure.authentication"
const CFG_APPLICATION = "application"
const CFG_ENV_STAGE = "env.stage"

/**
 * @package @leungas/aws-orm
 * @class File
 * @description this depict storage connection to S3
 * @author Mark Leung <leunga@gmail.com>
 */
export default class File {

    public folder: string = String()               /** @var {string} folder    this is our bucket name on S3 */
    public filename: string = String()             /** @var {string} filename  this is the file name we are after */
    public type: string = String()                 /** @var {string} type      the mime type of the file */
    public objectId: string = String()             /** @var {string} objectId  the eTag reference from S3 */
    
    /**
     * creating our instance
     * @return {void}
     */
    constructor () {}

    /**
     * get the url and content-type for the file
     * @return {object}
     */
    public get url() {
        if (this.objectId.length > 0) {
            if (this.filename && this.folder && this.type) {
                let setup: IAwsClient = Application.config(CFG_AWS_SETUP)
                let bucket = `${Application.config(CFG_APPLICATION)}-${Application.config(CFG_ENV_STAGE)}-${this.folder}`
                return {
                    location: `https://${bucket}.${setup.region}.amazon.com/${this.filename}`,
                    contentType: this.type
                }
            }
        } else 
            return undefined
    }

    /**
     * exporting the instance data
     * @return {object}
     */
    public export() : IDictionary {
        return {
            folder: this.folder,
            filename: this.filename,
            type: this.type,
            eTag: this.objectId
        }
    }

    /**
     * exporting the full file as buffer
     * @return {any}
     */
    public async fetch() : Promise<any> {
        if (this.objectId.length > 0) {
            if (this.url !== undefined) {
                let bucket = `${Application.config(CFG_APPLICATION)}-${Application.config(CFG_ENV_STAGE)}-${this.folder}`
                let client = new S3Client(bucket)
                let buffer = await client.get(this.filename)
                if (buffer !== undefined) 
                    return {
                        data: buffer,
                        type: this.type
                    }
                else 
                    return undefined
            } else
                return undefined
        } else 
            return undefined
    }

    /**
     * loading the data into the instance
     * @param {IDictionary} data        the data to load
     * @return {File}
     */
    public import(data: IDictionary) : File {
        Object.keys(data).map((value: string) => {
            switch (value) {
                case "filename": this.filename = data[value]; break
                case "folder": this.folder = data[value]; break
                case "eTag": this.objectId = data[value]; break
                case "type": this.type = data[value]; break
            }
        })
        return this
    }

    /**
     * loading the file from object 
     * @param {IDictionary} data        the data to load
     */
    public static load(data: IDictionary) : File {
        return (new File).import(data)
    }

    /**
     * storing the file into S3
     * @param {string} folder           the bucket on S3
     * @param {string} filename         the filename to store in S3
     * @param {string} type             MIME type of the file
     * @param {Buffer} data             the data stream we are storing
     * @return {File}
     */
    public async store(folder: string, filename: string, type: string, data: Buffer) : Promise<File> {
        let bucket = `${Application.config(CFG_APPLICATION)}-${Application.config(CFG_ENV_STAGE)}-${folder}`
        let client = new S3Client(bucket)
        let response = await client.put(filename, type, data)
        if (response !== undefined) {
            this.objectId = response.id 
            this.filename = filename
            this.type = type 
            this.folder = folder
            return this
        } else 
            throw new StorageFiledException(folder, filename)
    } 
}