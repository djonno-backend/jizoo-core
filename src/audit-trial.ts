import Application from './application'
import Model from './model'

/**
 * @package @jizo/accounts
 * @class AuditTrailEntry
 * @description this will record the source of the changes in the serivce for audit purposes
 * @author Mark Lenug <leungas@gmail.com>
 */
export default class AuditTrailEntry extends Model {

    public static tableName: string = "AuditTrailTable"

    /**
     * initiating the metadata
     * @return {void}
     */
    protected prepare() : void {
        super.prepare()
        this.rules.set("instance", {type: "text", required: true})
        this.rules.set("attributes", {type: "object", required: true})
        this.rules.set("call", {type: "text"})
        this.rules.set("user", {type: "text"})
        this.rules.set("code", {type: "number"})        
        this.rules.set("reason", {type: "text"})
    }

    /**
     * attribute table: get the full table name for DynamoDB
     * @return {string}
     */
    public static get table() : string {
        let service = Application.config("service")
        return Application.config("application") + "." + Application.config("env.stage") + "." + service + this.tableName
    }    

    /**
     * placing the data onto record
     * @param {Model} source    the entity we are recording change
     * @param {string} call     the function called this change
     * @param {string} user     the user that trigger the function
     * @return {AuditTrailEntry}
     */
    public record(source: Model, call?: string, user?: string) : AuditTrailEntry {
        this.set("instance", source.constructor.name)
        this.set("attributes", source.export())
        if (call !== undefined) this.set("call", call)
        if (user !== undefined) this.set("user", user)
        return this
    }
}