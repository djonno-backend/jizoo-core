import { Collection, IDictionary, Model } from '.'
import DynamoDBClient from './clients/database.client'
import IModelFilter from './interfaces/model.filter'

/**
 * @package @leungas/aws-orm
 * @class Query
 * @description the Query class allow us to more dynamically query object tables and return results
 * @author Mark Leung <leungas@gmail.com>
 */
export default class Query {

    private conditions: IModelFilter[] = []
    private transformer: any
    private module: any
    private sorts: any
    private table: string
    private target: string

    /**
     * initializing our instance
     * @param {module} namespace        the entire namespace of the serivce/app
     * @param {string} type             the instance class name to load as during query
     * @param {string} table            the table we are querying
     * @return {Query}
     */    
    constructor(namespace: any, type: string, table: string) {
        if (typeof namespace[type] !== undefined) {
            this.module = namespace
            this.table = table
            this.target = type
        } else  
            throw new Error(`Object type ${type} is not available. Unable to initiate Query`)
    }

    /**
     * collect the Collection object with items we need
     * @return {Collection}
     */
    private async execute() : Promise<Collection> {
        let client = new DynamoDBClient()        
        let data = await client.scan(this.table, this.conditions) as Array<IDictionary>
        if (this.sorts !== undefined) data = data.sort(this.sorts)
        return new Collection(data.map((item: IDictionary) => {
            let buffer: Model
            if (this.transformer !== undefined) {
                buffer = this.transformer(item) as Model
                buffer.import(data)
            } else 
                buffer = this.convert(data)
            return buffer
        }))
    }

    /**
     * converting data into proper objects then return
     * @param {IDictionary} data        the data source we received
     * @return {Model}
     */
    private convert(data: IDictionary) : any {
        let result = new this.module[this.target]
        result.import(data)    
        return result
    }

    /**
     * send back the first item available on the object list
     * @return {Model | undefined}
     */
    public async first() : Promise<any> {
        let data = await this.execute()
        return data.first()
    }

    /**
     * getting all instances back to the query
     * @return {Collection}
     */
    public async get() : Promise<Collection> {
        return await this.execute()
    }

    /**
     * send back the last element available on the object list
     */
    public async last() : Promise<any> {
        return (await this.execute()).last()
    }

    /**
     * assigning the sort determination function
     * @param {callback} compare    the function we use to compare objects
     * @return {Query} 
     */
    public sortBy(compare: (a: IDictionary, b: IDictionary) => number) : Query {
        this.sorts = compare
        return this
    }

    /**
     * @function transform
     * @description to set the transformer function if needed
     * @param {function} callback   the function to use to transform data to object
     * @return {Query}
     */
    public transform(callback: (source: object) => Model) : Query {
        this.transformer = callback
        return this
    }

    /**
     * adding filtering condition
     * @return {Query}
     */
    public where(filter: IModelFilter) : Query {
        this.conditions.push(filter)
        return this
    }
}